# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: nmatushe <nmatushe@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/03/29 12:29:11 by nmatushe          #+#    #+#              #
#    Updated: 2017/07/08 11:08:20 by vikovtun         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = rtv
LIB = libft/libft.a
SRCS =  main.c read_and_save.c ft_atoi_base.c
HEADERS = rtv.h
FLAGS = -Wall -Wextra -Werror -g
FLAGS_MLX = -lmlx -framework OpenGl -framework AppKit
BINS = $(SRCS:.c=.o)
all: $(NAME)
libclean:
	make -C libft/ clean
libfclean:
	make -C libft/ fclean
$(NAME): $(BINS)
	make -C libft/
	gcc -o $(NAME) $(BINS) $(FLAGS) $(FLAGS_MLX) $(LIB)
%.o: %.c
	gcc $(FLAGS) -c -o $@ $<
clean: libclean
	/bin/rm -f $(BINS)
fclean: libfclean clean
	/bin/rm -f $(NAME)
re: fclean all


# NAME = rtv
#
# HEAD = includes/
#
# VPATH = srcs:includes
#
# FLAGS = -O3 -Wall -Wextra -Werror -I $(HEAD)
#
# MLX = -lmlx -framework AppKit -framework OpenGl
#
# SRCS = main.c									\
# 		ft_atoi_base.c									\
#
# BINS = $(SRCS:.c=.o)
#
# all: $(NAME)
#
# $(NAME): $(BINS)
# 	 make re -C libft/ fclean && make -C libft/
# 	gcc -o $(NAME) -I libft/includes $(BINS) $(FLAGS) -L. libft/libft.a $(MLX)
#
# %.o: %.c
# 	gcc -I libft/includes $(FLAGS) -c -o $@ $<
#
# clean:
# 	/bin/rm -f $(BINS) && make -C libft/ fclean
#
# fclean: clean
# 	/bin/rm -f $(NAME)
#
# re: fclean all
