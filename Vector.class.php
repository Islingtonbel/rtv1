<?php

include 'Color.class.php';

class Vector {

	private $_x;
	private $_y;
	private $_z;
	private $_w = 0;

	public static $verbose = FALSE;

	public function __construct( array $arr ) {
		if (array_key_exists('orig', $arr)) {
			$tmp = $arr['orig'];
		}
		else {
			$tmp = new Vertex ( array('x' => 0.0, 'y' => 0.0, 'z' => 0.0, 'w' => 1 ) );
		}
		$this->_x = $arr['dest']->getX() - $tmp->getX();
		$this->_y = $arr['dest']->getY() - $tmp->getY();
		$this->_z = $arr['dest']->getZ() - $tmp->getZ();
		if ( self::$verbose )
			print( $this->__toString().' constructed'. PHP_EOL);
	}

	public function __destruct() {
		if ( self::$verbose )
			print( $this->__toString().' destructed'. PHP_EOL);
	}

	public function doc() {
		if (file_exists('Color.doc.txt')) {
			$filecontent = file_get_contents('Color.doc.txt');
			return $filecontent;
		}
	}

	public function __toString() {
		return (vsprintf('Vector( x:%.2f, y:%.2f, z:%.2f, w:%.2f )', array($this->_x, $this->_y, $this->_z, $this->_w)));
	}

	public function magnitude () {
		return (float)(sqrt(pow($this->_x, 2) + pow($this->_y, 2) + pow($this->_z, 2)));
	}

	public function normalize() {
		$tmp = $this->magnitude();
		$x = $this->_x / $tmp; 
		$y = $this->_y / $tmp;
		$z = $this->_z / $tmp;
		return (new Vector ( array( 'dest' => new Vertex( array('x' => $x, 'y' => $y, 'z' => $z ) ) ) ));
	}

	public function add(Vector $rhs) {
		$x = $this->_x + $rhs->_x;
		$y = $this->_y + $rhs->_y;
		$z = $this->_z + $rhs->_z;
		return (new Vector ( array( 'dest' => new Vertex( array('x' => $x, 'y' => $y, 'z' => $z ) ) ) ));
	}

	public function sub(Vector $rhs) {
		$x = $this->_x - $rhs->_x;
		$y = $this->_y - $rhs->_y;
		$z = $this->_z - $rhs->_z;
		return (new Vector ( array( 'dest' => new Vertex( array('x' => $x, 'y' => $y, 'z' => $z ) ) ) ));
	}

	public function opposite() {
		$x = $this->_x * -1;
		$y = $this->_y * -1;
		$z = $this->_z * -1;
		return (new Vector ( array( 'dest' => new Vertex( array('x' => $x, 'y' => $y, 'z' => $z ) ) ) ));
	}

	public function scalarProduct($k) {
		$x = $this->_x * $k;
		$y = $this->_y * $k;
		$z = $this->_z * $k;
		return (new Vector ( array( 'dest' => new Vertex( array('x' => $x, 'y' => $y, 'z' => $z ) ) ) ));
	}

	public function dotProduct(Vector $rhs) {
		return ((float)($this->_x * $rhs->_x) + ($this->_y * $rhs->_y) + ($this->_z * $rhs->_z) );
	}

	public function cos(Vector $rhs) {
		$cos = $this->dotProduct($rhs) / ($this->magnitude() * $rhs->magnitude());
		return $cos;
	}

	public function crossProduct(Vector $rhs) {
		$x = $this->_y * $rhs->_z - $this->_z * $rhs->_y;
		$y = $this->_z * $rhs->_x - $this->_x * $rhs->_z;
		$z = $this->_x * $rhs->_y - $this->_y * $rhs->_x;
		return (new Vector ( array( 'dest' => new Vertex( array('x' => $x, 'y' => $y, 'z' => $z ) ) ) ));
	}

	public function __get( $abir) {
		return "abir";
	}

	public function getX() {
		return $_x;
	}

	public function getY() {
		return $_y;
	}

	public function getZ() {
		return $_z;
	}

	public function getW() {
		return $_w;
	}

}


?>