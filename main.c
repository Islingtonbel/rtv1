/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/07 11:33:31 by vikovtun          #+#    #+#             */
/*   Updated: 2017/07/08 11:29:01 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

static int		key_press(int key, t_s *p)
{
	key == 53 ? mlx_clear_window(p->mlx, p->win) : 0;
	key == 53 ? mlx_destroy_window(p->mlx, p->win) : 0;
	key == 53 ? exit(0) : 0;

	key == 124 ? p->fig[3]->coor->x += 10 : 0;
	key == 123 ? p->fig[3]->coor->x -= 10 : 0;
	key == 69 ? p->fig[3]->coor->y -= 10 : 0;
	key == 78 ? p->fig[3]->coor->y += 10 : 0;
	key == 126 ? p->fig[3]->coor->z -= 10 : 0;
	key == 125 ? p->fig[3]->coor->z += 10 : 0;

	printf("x: %f\n", p->fig[3]->coor->x);
	printf("y: %f\n", p->fig[3]->coor->y);
	printf("z: %f\n", p->fig[3]->coor->z);
	mlx_clear_window(p->mlx, p->mlx);
	ft_sphere(p);
	return (0);
}

int	main(int argc, char** argv)
{
	char	***fig;
	t_s *p;

	if (argc != 2)
		return (0);
	ft_read(argv[1], &fig);
	p = (t_s *)malloc(sizeof(t_s));
	ft_save(fig, p);
	p->mlx = mlx_init();
	p->win = mlx_new_window(p->mlx, W, H, "rtv1");
	// ft_plane(p);
	ft_sphere(p);

	mlx_key_hook(p->win, key_press, p);
	// mlx_hook(p->mlx, 4, 5, mouse_fig_save, p);
	// mlx_hook(p->win, 6, (1L << 6), mouse_move_hook, p);
	mlx_loop(p->mlx);
}
