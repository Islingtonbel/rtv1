/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 11:28:56 by vikovtun          #+#    #+#             */
/*   Updated: 2017/07/08 12:42:25 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void ft_sphere(t_s p)
{
	int	dx; // dest
	int	dy;
	int	dz;
	int	x0; // P0, start
	int	y0;
	int	z0;
	int	x1; // P1, pixel coor / direction
	int	y1;
	int	z1;
	int winx;
	int winy;
	int t;

	x0 = p->camcoor->x;
	y0 = p->camcoor->y;
	z0 = p->camcoor->z;

	cx = p->fig[3]->coor->x;
	cy = p->fig[3]->coor->y;
	cz = p->fig[3]->coor->z;

	dx = x1 - x0;
	dy = y1 - y0;
	dz = z1 - z0;

	winy = -1;
	while (++winy != H)
	{
		winx = -1;
		while (++winx != W)
		{
			a = (winx - p->camcoor->x) * (winx - p->camcoor->x) + (winy - p->camcoor->y) * (winy - p->camcoor->y) + (winz - p->camcoor->z) * (0 - p->camcoor->z);
			b = 2 * (winx - p->camcoor->x) * (p->camcoor->x - p->fig[3]->coor->x) + 2 * (winy - p->camcoor->y) * (p->camcoor->y - p->fig[3]->coor->y) + 2 * (0 - p->camcoor->z) * (p->camcoor->z - p->fig[3]->coor->z);
			c = cx * cx + cy * cy + cz * cz + x0 * x0 + y0 * y0 + z0 * z0 + - 2 * (cx * x0 + cy * y0 + cz * z0) - R * R;
			d = b * b - 4 * a * c;
			if (d >= 0)
			{
				t1 = (-b - sqrt(d) / 2 * a);
				t2 = (-b + sqrt(d) / 2 * a);
				t = (t1 < t2) ? t1 : t2;
				inx = x0 + t * dx;
				iny = y0 + t * dy;
				inz = z0 + t * dz;

				t??
			}
		}

	}
}

a = dx * dx + dy * dy + dz * dz;
b = 2 * dx * (x0-cx) + 2 * dy * (y0 - cy) + 2 * dz * (z0 - cz);
c = cx * cx + cy * cy + cz * cz + x0 * x0 + y0 * y0 + z0 * z0 +
							 - 2 * (cx * x0 + cy * y0 + cz * z0) - R * R;
d = b * b - 4 * a * c;
