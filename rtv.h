/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rtv.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/07 11:13:45 by vikovtun          #+#    #+#             */
/*   Updated: 2017/07/08 11:11:41 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RTV_H
# define RTV_H

# include "./libft/libft.h"
# include <mlx.h>
# include <math.h>
# include <fcntl.h>
# include <time.h>
#include <stdio.h>

# define W 1000
# define H 1000

typedef struct	s_xyz
{
	float		x;
	float		y;
	float		z;
}				t_xyz;

typedef	struct	s_fig
{
	t_xyz 		*coor;
	t_xyz		*dir;
	int 		radius;
	int 		height;
	int 		color;
	int 		fig;
	int			alpha;
}				t_fig;

typedef struct	s_s
{
	t_fig		**fig;
	t_xyz		*camcoor;
	t_xyz		*camdir;
	t_xyz		**light;
	t_xyz		*raypos;
	t_xyz		*raydir;
	void 		*mlx;
	void 		*win;
	void 		*img;
	int			fig_count;
	int			light_count;
}				t_s;

void			ft_read(char *argv, char ****fig);
void			ft_save(char ***fig, t_s *p);
void			ft_err(void);
int				ft_atoi_base(char *str, int base);

#endif
