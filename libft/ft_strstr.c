/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/20 02:33:16 by vikovtun          #+#    #+#             */
/*   Updated: 2017/01/03 00:42:38 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	int a;
	int b;
	int c;

	a = 0;
	c = a;
	if (little[0] == 0)
		return ((char *)big);
	while (big[a] != '\0' || big[a] != '\0')
	{
		b = 0;
		if (big[a] == little[b])
		{
			c = a;
			while (big[c] == little[b] && little[b])
			{
				c++;
				b++;
			}
			if (little[b] == '\0')
				return ((char *)big + a);
		}
		a++;
	}
	return (NULL);
}
