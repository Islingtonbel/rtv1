/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/16 04:55:35 by vikovtun          #+#    #+#             */
/*   Updated: 2017/01/03 01:39:42 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int	ft_first(const char *s)
{
	unsigned long	i;
	char			*blank;

	i = 0;
	while ((s[i] == ' ' || s[i] == '\n' || s[i] == '\t') && s[i])
		i++;
	if (i == ft_strlen(s))
	{
		blank = (char *)malloc(sizeof(char));
		blank[0] = '\0';
		return ((char)blank[0]);
	}
	return (i);
}

static int	ft_last(const char *s)
{
	int				i;

	i = ft_strlen(s) - 1;
	while ((s[i] == ' ' || s[i] == '\n' || s[i] == '\t')
		&& ((i) > 0))
		i--;
	return (i);
}

char		*ft_strtrim(char const *s)
{
	char			*result;
	size_t			first;
	size_t			last;
	int				i;
	unsigned int	size2;

	if (s == 0)
		return (NULL);
	first = ft_first(s);
	last = ft_last(s) + 1;
	if (!(result = (char *)malloc(sizeof(char) * (last - first + 1))))
		return (0);
	i = 0;
	while ((s[i] == ' ' || s[i] == '\n'
			|| s[i] == '\t') && s[i])
		i++;
	size2 = 0;
	while (size2 < (last - first))
	{
		result[size2] = s[size2 + i];
		size2++;
	}
	result[size2] = 0;
	return (result);
}
