/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/21 03:09:01 by vikovtun          #+#    #+#             */
/*   Updated: 2017/01/06 17:31:02 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int		word_counter(char const *s, char c)
{
	int	words;
	int	i;

	i = 0;
	words = 0;
	while (s[i] != 0)
	{
		while (s[i] == c)
			i++;
		if (s[i])
			words++;
		while (s[i] && s[i] != c)
			i++;
	}
	return (words);
}

static	char	*new_word(char const *s, char c)
{
	int		test;
	int		size;
	char	*temp;

	test = 0;
	size = 0;
	while (s[size] != 0 && s[size] != c)
		size++;
	if (!(temp = (char *)malloc(sizeof(char *) * (size + 1))))
		return (NULL);
	while (s[test] && s[test] != c)
	{
		temp[test] = s[test];
		test++;
	}
	temp[test] = 0;
	return (temp);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**words;
	int		i;
	int		j;
	int		count;

	if (s == 0)
		return (NULL);
	count = word_counter(s, c);
	if (!(words = (char **)malloc(sizeof(char *) * (count + 1))))
		return (NULL);
	i = 0;
	j = 0;
	while (s[j])
	{
		while (s[j] == c)
			j++;
		if (s[j] == 0)
			break ;
		words[i] = new_word(s + j, c);
		i++;
		while (s[j] != c && s[j])
			j++;
	}
	words[i] = 0;
	return (words);
}
