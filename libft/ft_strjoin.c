/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/16 04:21:26 by vikovtun          #+#    #+#             */
/*   Updated: 2017/03/09 13:54:18 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*res;

	if (!s1 && !s2)
		return (NULL);
	else if (!s1)
		return (ft_strdup(s2));
	else if (!s2)
		return (ft_strdup(s1));
	res = ft_strnew(ft_strlen((char *)s1) + ft_strlen((char *)s2));
	if (!res)
		return (0);
	res = ft_strcat(res, s1);
	res = ft_strcat(res, s2);
	return (res);
}
