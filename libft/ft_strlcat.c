/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/20 01:48:20 by vikovtun          #+#    #+#             */
/*   Updated: 2017/01/03 14:49:06 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	i;
	size_t	j;
	size_t	dst_len;
	size_t	src_len;

	i = 0;
	j = 0;
	while (src[i])
		i++;
	src_len = i;
	i = 0;
	while (dst[i])
		i++;
	dst_len = i;
	if ((int)(size - dst_len - 1) < 0)
		return (src_len + size);
	while (src[j] && j < size - dst_len - 1)
		dst[i++] = src[j++];
	dst[i] = '\0';
	return (dst_len + ft_strlen(src));
}
