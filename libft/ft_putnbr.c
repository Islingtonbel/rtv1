/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/15 23:55:03 by vikovtun          #+#    #+#             */
/*   Updated: 2017/01/06 16:17:10 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr(int n)
{
	long	temp;
	long	size;

	if (n == -2147483648)
	{
		ft_putstr("-2147483648");
		return ;
	}
	if (n < 0)
	{
		ft_putchar('-');
		n = -n;
	}
	temp = n;
	size = 1;
	while ((temp /= 10) > 0)
		size *= 10;
	temp = n;
	while (size)
	{
		ft_putchar((char)(temp / size) + 48);
		temp %= size;
		size /= 10;
	}
}
