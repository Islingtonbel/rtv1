/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_newline.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/03 15:39:11 by vikovtun          #+#    #+#             */
/*   Updated: 2017/01/03 16:39:27 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_newline(char *str, int i)
{
	while (str[i] != '\n')
		i++;
	return (i);
}
