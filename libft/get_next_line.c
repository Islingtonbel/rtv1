/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/23 14:41:56 by vikovtun          #+#    #+#             */
/*   Updated: 2017/07/08 11:11:30 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list			*allocate_list(t_list **head, int fd)
{
	t_list *current;

	current = *head;
	while (current != NULL)
	{
		if (fd == (int)current->content_size)
			return (current);
		current = current->next;
	}
	current = ft_lstnew("\0", 1);
	current->content_size = fd;
	current->next = *head;
	*head = current;
	return (current);
}

char			*ft_join(int n, char *s1, char *s2)
{
	char	*str;
	char	*holder1;
	char	*holder2;

	holder1 = s1;
	holder2 = s1;
	if (!s1 && !s2)
		return (NULL);
	else if (!s1)
		return (ft_strdup(s2));
	else if (!s2)
		return (ft_strdup(s1));
	str = ft_strnew(ft_strlen(s1) + ++n);
	holder2 = str;
	while (*s1)
		*str++ = *s1++;
	while (*s2 && n-- > 1)
		*str++ = *s2++;
	*str = '\0';
	free(holder1);
	return (holder2);
}

int				get_next_line(int const fd, char **line)
{
	static	t_list	*list;
	t_list			*head;
	char			b[BUFF_SIZE + 1];
	int				i;
	char			*holder;

	(list == NULL) ? (list = allocate_list(&list, fd)) : 0;
	if ((fd < 0) || ((i = read(fd, &b, 0)) < 0))
		return (-1);
	head = list;
	list = allocate_list(&head, fd);
	while (!ft_strchr(list->content, '\n') && (i = read(fd, b, BUFF_SIZE)))
		list->content = ft_join(i, list->content, b);
	i = 0;
	while (((char *)list->content)[i] && ((char *)list->content)[i] != '\n')
		++i;
	*line = ft_strsub(list->content, 0, i);
	holder = list->content;
	((char *)list->content)[i] == '\n' ?
	(list->content = ft_strdup(list->content + ++i)) :
	(list->content = ft_strdup(list->content + i));
	free(holder);
	list = head;
	return (i ? 1 : 0);
}
