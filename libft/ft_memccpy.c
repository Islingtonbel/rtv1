/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/19 23:42:59 by vikovtun          #+#    #+#             */
/*   Updated: 2017/01/03 15:21:57 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t				i;
	unsigned char		buff;

	i = 0;
	while (n > 0)
	{
		buff = ((unsigned char *)src)[i];
		((unsigned char *)dst)[i] = ((unsigned char *)src)[i];
		i++;
		if (buff == (unsigned char)c)
			return (&dst[i]);
		n--;
	}
	return (NULL);
}
