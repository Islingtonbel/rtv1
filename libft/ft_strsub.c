/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/16 02:07:18 by vikovtun          #+#    #+#             */
/*   Updated: 2017/01/03 15:34:55 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	size_t	i;
	int		j;
	char	*res;

	j = 0;
	i = (size_t)start;
	if (!s)
		return (0);
	res = (char *)malloc((len + 1) * sizeof(char));
	if (!res)
		return (0);
	while (i < (start + len))
	{
		res[j] = ((char *)s)[i];
		j++;
		i++;
	}
	res[j] = '\0';
	return (res);
}
