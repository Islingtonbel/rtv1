/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/21 03:09:00 by vikovtun          #+#    #+#             */
/*   Updated: 2017/01/03 01:48:56 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int	ft_len(int i)
{
	int	len;

	len = 0;
	while (i /= 10)
	{
		len++;
	}
	return (len);
}

char		*ft_itoa(int n)
{
	char	*result;
	int		len;
	int		i;

	i = n;
	len = ((n < 0) ? 2 : 1) + ft_len(n);
	result = (char *)malloc(sizeof(char) * (len + 1));
	if (!result)
		return (0);
	else if (n < 0)
		result[0] = '-';
	else if (n == 0)
		result[0] = '0';
	result[len] = '\0';
	while (len--)
	{
		if (n >= 0)
			result[len] = '0' + (n % 10);
		if (n < 0)
			result[len] = '0' - (n % 10);
		n /= 10;
		if (n == 0)
			break ;
	}
	return (result);
}
