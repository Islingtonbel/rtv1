/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/20 02:52:39 by vikovtun          #+#    #+#             */
/*   Updated: 2017/01/09 09:01:11 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	i;
	size_t	j;

	i = 0;
	if (!little[0])
		return ((char*)big);
	while ((i != len) && (little[i] != '\0' || big[i] != '\0'))
	{
		j = 0;
		while (big[i + j] == little[j] && i + j < len)
		{
			if (little[j + 1] == '\0')
				return ((char*)&big[i]);
			j++;
		}
		if (big[i] == '\0')
			break ;
		i++;
	}
	if (little[0] == '\0')
		return ((char*)big);
	return (0);
}
