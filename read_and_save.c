/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_and_save.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vikovtun <vikovtun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/08 10:45:18 by vikovtun          #+#    #+#             */
/*   Updated: 2017/07/08 12:42:23 by vikovtun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rtv.h"

void		ft_err(void)
{
	ft_putendl("error :)");
	exit(0);
}

void	ft_save(char ***fig, t_s *p)
{
	int	i;
	int j;
	char **tmp;

	i = -1;
	j = 0;
	while (fig[++i] != NULL)
	{
		ft_strequ(fig[i][0], "sphere") == 1 ? j++ : 0;
		ft_strequ(fig[i][0], "cone") == 1 ? j++ : 0;
		ft_strequ(fig[i][0], "plane") == 1 ? j++ : 0;
		ft_strequ(fig[i][0], "cylinder") == 1 ? j++ : 0;
	}

	p->fig = (t_fig **)malloc(sizeof(t_fig *) * j);
	p->fig_count = j;

	i = -1;
	j = 0;
	while (fig[++i] != NULL)
		ft_strequ(fig[i][0], "light") == 1 ? j++ : 0;
	p->light = (t_xyz **)malloc(sizeof(t_xyz *) * j);
	p->light_count = j;

	p->camcoor = (t_xyz *)malloc(sizeof(t_xyz));
	p->camdir = (t_xyz *)malloc(sizeof(t_xyz));

	i = 0;
	j = 0;
	int c = -1;
	while (fig[++c])
	{
		if (ft_strequ(fig[c][0], "sphere") == 1)
		{
			p->fig[i] = (t_fig *)malloc(sizeof(t_fig));
			p->fig[i]->coor = (t_xyz *)malloc(sizeof(t_xyz));
			p->fig[i]->fig = 1;
			p->fig[i]->coor->x = ft_atoi(fig[c][1]);
			p->fig[i]->coor->y = ft_atoi(fig[c][2]);
			p->fig[i]->coor->z = ft_atoi(fig[c][3]);
			p->fig[i]->radius = ft_atoi(fig[c][4]);
			tmp = ft_strsplit(fig[c][5], 'x');
			p->fig[i]->color = ft_atoi_base(tmp[1], 16);
			free(tmp);
			i++;
		}
		else if (ft_strequ(fig[c][0], "cylinder") == 1)
		{
			p->fig[i] = (t_fig *)malloc(sizeof(t_fig));
			p->fig[i]->coor = (t_xyz *)malloc(sizeof(t_xyz));
			p->fig[i]->dir = (t_xyz *)malloc(sizeof(t_xyz));
			p->fig[i]->fig = 2;
			p->fig[i]->coor->x = ft_atoi(fig[c][1]);
			p->fig[i]->coor->y = ft_atoi(fig[c][2]);
			p->fig[i]->coor->z = ft_atoi(fig[c][3]);
			p->fig[i]->dir->x = ft_atoi(fig[c][4]);
			p->fig[i]->dir->y = ft_atoi(fig[c][5]);
			p->fig[i]->dir->z = ft_atoi(fig[c][6]);
			p->fig[i]->radius = ft_atoi(fig[c][7]);
			p->fig[i]->height = ft_atoi(fig[c][8]);
			tmp = ft_strsplit(fig[c][9], 'x');
			p->fig[i]->color = ft_atoi_base(tmp[1], 16);
			free(tmp);
			i++;
		}
		else if (ft_strequ(fig[c][0], "cone") == 1)
		{
			p->fig[i] = (t_fig *)malloc(sizeof(t_fig));
			p->fig[i]->coor = (t_xyz *)malloc(sizeof(t_xyz));
			p->fig[i]->dir = (t_xyz *)malloc(sizeof(t_xyz));
			p->fig[i]->fig = 3;
			p->fig[i]->coor->x = ft_atoi(fig[c][1]);
			p->fig[i]->coor->y = ft_atoi(fig[c][2]);
			p->fig[i]->coor->z = ft_atoi(fig[c][3]);
			p->fig[i]->dir->x = ft_atoi(fig[c][4]);
			p->fig[i]->dir->y = ft_atoi(fig[c][5]);
			p->fig[i]->dir->z = ft_atoi(fig[c][6]);
			p->fig[i]->height = ft_atoi(fig[c][7]);
			p->fig[i]->alpha = ft_atoi(fig[c][8]);
			tmp = ft_strsplit(fig[c][9], 'x');
			p->fig[i]->color = ft_atoi_base(tmp[1], 16);
			free(tmp);
			i++;
		}

		else if (ft_strequ(fig[c][0], "plane") == 1)
		{
			p->fig[i] = (t_fig *)malloc(sizeof(t_fig));
			p->fig[i]->coor = (t_xyz *)malloc(sizeof(t_xyz));
			p->fig[i]->dir = (t_xyz *)malloc(sizeof(t_xyz));
			p->fig[i]->fig = 4;
			p->fig[i]->coor->x = ft_atoi(fig[c][1]);
			p->fig[i]->coor->y = ft_atoi(fig[c][2]);
			p->fig[i]->coor->z = ft_atoi(fig[c][3]);
			p->fig[i]->dir->x = ft_atoi(fig[c][4]);
			p->fig[i]->dir->y = ft_atoi(fig[c][5]);
			p->fig[i]->dir->z = ft_atoi(fig[c][6]);
			tmp = ft_strsplit(fig[c][7], 'x');
			p->fig[i]->color = ft_atoi_base(tmp[1], 16);
			free(tmp);
			i++;
		}
		else if (ft_strequ(fig[c][0], "light") == 1)
		{
			p->light[j] = (t_xyz *)malloc(sizeof(t_xyz));
			p->light[j]->x = ft_atoi(fig[c][1]);
			p->light[j]->y = ft_atoi(fig[c][2]);
			p->light[j]->z = ft_atoi(fig[c][3]);
			j++;
		}
		else if (ft_strequ(fig[c][0], "camera") == 1)
		{
			p->camcoor->x = ft_atoi(fig[c][1]);
			p->camcoor->y = ft_atoi(fig[c][2]);
			p->camcoor->z = ft_atoi(fig[c][3]);
			p->camdir->x = ft_atoi(fig[c][4]);
			p->camdir->y = ft_atoi(fig[c][5]);
			p->camdir->z = ft_atoi(fig[c][6]);
		}
	}
	i = -1;
	while (++i != p->fig_count)
	{
	printf("%i\n", p->fig[i]->fig);
	printf("%f\n", p->fig[i]->coor->x);
	printf("%f\n", p->fig[i]->coor->y);
	printf("%f\n", p->fig[i]->coor->z);
	printf("%i\n", p->fig[i]->color);
	}
	printf("LIGHTS:\n");
	printf("%f\n", p->light[0]->x);
	printf("%f\n", p->light[0]->y);
	printf("%f\n", p->light[0]->z);
	printf("%f\n", p->light[1]->x);
	printf("%f\n", p->light[1]->y);
	printf("%f\n", p->light[1]->z);
	printf("CAMERA POSITION:\n");
	printf("%f\n", p->camcoor->x);
	printf("%f\n", p->camcoor->y);
	printf("%f\n", p->camcoor->z);
	printf("CAMERA ANGLE:\n");
	printf("%f\n", p->camdir->x);
	printf("%f\n", p->camdir->y);
	printf("%f\n", p->camdir->z);
}

void	ft_read(char *argv, char ****fig)
{
	int		fd;
	int		i;
	int 	w;
	int 	flag;
	char	*tmp;
	char	**map;

	w = -1;
	(fd = open(argv, O_RDONLY)) < 0 ? ft_err() : 0;
	while ((flag = get_next_line(fd, &tmp)))
	{
		++w;
		free(tmp);
	}
	close(fd);
	w++;
	i = -1;
	(fd = open(argv, O_RDONLY)) < 0 ? ft_err() : 0;
	map = (char **)malloc(sizeof(char *) * (w + 1));
	while (++i < w)
		flag = get_next_line(fd, &(map)[i]);
	close(fd);
	(map)[i++] = NULL;
	i = -1;
	*fig = (char ***)malloc(sizeof(char **) * (w + 1));
	while (++i < w)
		(*fig)[i] = ft_strsplit((map)[i], ' ');
	(*fig)[i] = NULL;
	i = -1;
	while (++i < w)
		free((map)[i]);
	free(map);
}
